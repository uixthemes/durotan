var durotan = durotan || {};

(function( $ ) {
    'use strict';
	/**
	 * The main function to init the theme
	 */
	durotan.init = function() {
		this.toggleOffCanvas();
		this.toggleModals();
		this.instanceSearch();
		this.focusSearchField();
		this.headerVertical();
		this.navSmartDot();

		this.postsFeaturedCarousel();
		this.postsLatestCarousel();
	};

	durotan.postsFeaturedCarousel = function(){
		if ( typeof Swiper === 'undefined' ) {
			return;
		}

		var container = $('.durotan-featured-posts-carousel'),
				options = {
					loop            : true,
					autoplay        : false,
					delay           : 800,
					speed           : 1000,
					watchOverflow   : true,
					lazy            : false,
					effect          : 'fade',
					fadeEffect: {
						crossFade: true
					},
					pagination: {
						el: container.find('.swiper-pagination'),
						type: 'bullets',
						clickable: true
					},
					on              : {
						init: function() {
							container.css( 'opacity', 1 );
						}
					}
				};

		new Swiper( container, options );

	};

	durotan.postsLatestCarousel = function() {
		if ( typeof Swiper === 'undefined' ) {
			return;
		}

		var container = $('.durotan-latest-posts-carousel'),
				options = {
					slidesPerView: 2,
					slidesPerGroup: 2,
					spaceBetween: 60,
					loop            : false,
					autoplay        : false,
					delay           : 800,
					speed           : 1000,
					watchOverflow   : true,
					lazy            : false,
					navigation: {
						nextEl: container.find('.durotan-posts-button-next'),
						prevEl: container.find('.durotan-posts-button-prev'),
					},
					fadeEffect: {
						crossFade: true
					},
					on              : {
						init: function() {
							container.css( 'opacity', 1 );
						}
					},
					breakpoints: {
						0: {
							slidesPerView: 1,
							slidesPerGroup: 1,
							spaceBetween: 0,
						},
						480: {
							slidesPerView: 2,
							slidesPerGroup: 2,
							spaceBetween: 30,
						}
					}
				};

		new Swiper( container, options );
	};

	/**
	 * Open quick links when focus on search field
	 */
	durotan.focusSearchField = function() {
		$( '.header-search .search-field' ).on( 'focus', function() {
			var $quicklinks = $( this ).closest( '.header-search' ).find( '.quick-links' );

			if ( !$quicklinks.length ) {
				return;
			}

			$quicklinks.addClass( 'open' );
			$( this ).addClass( 'focused' );
		} );

		$( document.body ).on( 'click', '*', function( event ) {
			var $target = $( event.target );

			if ( $target.is( '.header-search' ) || $target.closest( '.header-search' ).length ) {
				return;
			}

			$( '.quick-links', '.header-search' ).removeClass( 'open' );
			$( '.search-field', '.header-search' ).removeClass( 'focused' );
		} );
	};

	durotan.instanceSearch = function() {
		var $modal = $( '#search-modal' ),
			$header = $modal.find( '.modal__header' ),
			$content = $modal.find( '.modal__content' ),
			$result = $modal.find( '.search-result' ),
			$form = $modal.find( 'form' ),
			$search = $form.find( 'input.search-field' );

		if ( $result.find( '.search-result__items' ).length && typeof PerfectScrollbar !== 'undefined' ) {
			new PerfectScrollbar( $result.find( '.search-result__items' ).get( 0 ), {
				suppressScrollX: true
			} );
		}

		// Focus on the search field when search modal opened
		$( document.body ).on( 'durotan_modal_opened', function( event, target ) {
			if ( target.is( '#search-modal' ) ) {
				$search.focus();
			}
		} );

		if ( $modal.hasClass( 'searched' ) ) {
			$content.css( 'top', $header.outerHeight() );
		}
	}

	/**
	 * Toggle off-screen panels
	 */
	durotan.toggleOffCanvas = function() {
		$( document.body ).on( 'click', '[data-toggle="off-canvas"]', function( event ) {
			var target = '#' + $( this ).data( 'target' );

			if ( $( target ).hasClass( 'open' ) ) {
				durotan.closeOffCanvas( target );
			} else if ( durotan.openOffCanvas( target ) ) {
				event.preventDefault();
			}
		} ).on( 'click', '.offscreen-panel__button-close, .offscreen-panel__backdrop', function( event ) {
			event.preventDefault();

			durotan.closeOffCanvas( this );
		} ).on( 'keyup', function ( e ) {
			if ( e.keyCode === 27 ) {
				durotan.closeOffCanvas();
			}
		} );
	};

	/**
	 * Open off canvas panel.
	 * @param string target Target selector.
	 */
	durotan.openOffCanvas = function( target ) {
		var $target = $( target );

		if ( !$target.length ) {
			return false;
		}

		$target.fadeIn();
		$target.addClass( 'open' );

		$( document.body ).addClass( 'offcanvas-opened ' + $target.attr( 'id' ) + '-opened' ).trigger( 'durotan_off_canvas_opened', [$target] );

		return true;
	}

	/**
	 * Close off canvas panel.
	 * @param DOM target
	 */
	durotan.closeOffCanvas = function( target ) {
		if ( !target ) {
			$( '.offscreen-panel' ).each( function() {
				var $panel = $( this );

				if ( ! $panel.hasClass( 'open' ) ) {
					return;
				}

				$panel.removeClass( 'open' ).fadeOut();
				$( document.body ).removeClass( $panel.attr( 'id' ) + '-opened' );
			} );
		} else {
			target = $( target ).closest( '.offscreen-panel' );
			target.removeClass( 'open' ).fadeOut();

			$( document.body ).removeClass( target.attr( 'id' ) + '-opened' );
		}

		$( document.body ).removeClass( 'offcanvas-opened' ).trigger( 'durotan_off_canvas_closed', [target] );
	}

	/**
	 * Toggle modals.
	 */
	durotan.toggleModals = function() {
		$( document.body ).on( 'click', '[data-toggle="modal"]', function( event ) {
			var target = '#' + $( this ).data( 'target' );

			if ( $( target ).hasClass( 'open' ) ) {
				durotan.closeModal( target );
			} else if ( durotan.openModal( target ) ) {
				event.preventDefault();
			}
		} ).on( 'click', '.modal__button-close, .modal__backdrop', function( event ) {
			event.preventDefault();

			durotan.closeModal( this );
		} ).on( 'keyup', function ( e ) {
			if ( e.keyCode === 27 ) {
				durotan.closeModal();
			}
		} );
	};

	/**
	 * Open a modal.
	 *
	 * @param string target
	 */
	durotan.openModal = function( target ) {
		var $target = $( target );

		if ( !$target.length ) {
			return false;
		}

		$target.fadeIn();
		$target.addClass( 'open' );

		$( document.body ).addClass( 'modal-opened ' + $target.attr( 'id' ) + '-opened' ).trigger( 'durotan_modal_opened', [$target] );

		return true;
	}

	/**
	 * Close a modal.
	 *
	 * @param string target
	 */
	durotan.closeModal = function( target ) {
		if ( !target ) {
			$( '.modal' ).removeClass( 'open' ).fadeOut();

			$( '.modal' ).each( function() {
				var $modal = $( this );

				if ( ! $modal.hasClass( 'open' ) ) {
					return;
				}

				$modal.removeClass( 'open' ).fadeOut();
				$( document.body ).removeClass( $modal.attr( 'id' ) + '-opened' );
			} );
		} else {
			target = $( target ).closest( '.modal' );
			target.removeClass( 'open' ).fadeOut();

			$( document.body ).removeClass( target.attr( 'id' ) + '-opened' );
		}

		$( document.body ).removeClass( 'modal-opened' ).trigger( 'durotan_modal_closed', [target] );
	}

	/**
	 * Header Vertical Scroll
	 */
	durotan.headerVertical = function() {
		var $headerVertical = $( '.header-v10' );

		if ( ! $headerVertical.length ) {
			return;
		}

		if ( typeof PerfectScrollbar !== 'undefined' ) {
			new PerfectScrollbar( $( '.header-v10' ).find( '.header__wrapper' ).get( 0 ) );
		}
	}

	/**
	 * Smart Dots
	 */
	durotan.navSmartDot = function() {
		var $header = $( '#masthead' ),
			$mainNav = $( '.main-navigation', $header ),
			$menu = $( 'ul.menu', $mainNav );

			console.log(111);

		if ( ! $header.hasClass( 'header-has-smart-dot' ) ) {
			return;
		}

		if ( ! $mainNav.length ) {
			return;
		}

		$header.append( '<div id="durotan-smart-dot" class="durotan-smart-dot header__smart-dot"></div>' );

		var $smartDot = $( '#durotan-smart-dot' ),
			$menuItem = $menu.children(),
			$currentMenuItem = $menu.children( 'li.current-menu-item' ),
			oriPosLeft = $currentMenuItem.offset().left + $currentMenuItem.outerWidth() / 2 - $smartDot.outerWidth() / 2;

		$smartDot
			.data( 'left', oriPosLeft )
			.css( 'left', oriPosLeft );

		$menuItem.mouseover( function() {
			var $el = $( this ),
				posLeft = $el.offset().left + $el.outerWidth() / 2 - $smartDot.outerWidth() / 2;

			$smartDot.css( 'left', posLeft );
		} );

		$menuItem.mouseleave( function() {
			var posLeft = $smartDot.data( 'left' );

			$smartDot.css( 'left', posLeft );
		});

		$menuItem.on( 'click', function() {
			var $el = $( this ),
				posLeft = $el.offset().left + $el.outerWidth() / 2 - $smartDot.outerWidth() / 2;

			$smartDot
				.data( 'left', posLeft )
				.css( 'left', posLeft );
		} );
	}

    /**
	 *    Fire when document ready
	 */
	$( function() {
		durotan.init();
	} );
})( jQuery );