var durotan = durotan || {};

(function( $ ) {
    'use strict';
	/**
	 * The main function to init the theme
	 */
	durotan.init = function() {
		this.sliderHomepage();
		this.popupVideo();
		this.sliderTabs();
    };

	durotan.sliderHomepage = function() {
		if ( typeof Swiper === 'undefined' ) {
			return;
		}

		var container = $('.durotan-slider-carousel'),
				options = {
					slidesPerView: 'auto',
					//slidesPerView: 1,
					slidesPerGroup: 1,
					loop            : false,
					autoplay        : false,
					delay           : 800,
					speed           : 1000,
					watchOverflow   : true,
					lazy            : false,
					pagination: {
						el: container.find('.durotan-slide__pagination'),
						clickable: true,
						renderBullet: function (index, className) {
							return '<span class="durotan-slide__pagination-bullet ' + className + '"></span>';
						},
					},
					navigation: {
						nextEl: container.find('.durotan-slide__arrow-button--next'),
						prevEl: container.find('.durotan-slide__arrow-button--prev'),
					},
					fadeEffect: {
						crossFade: true
					},
					on              : {
						init: function() {
							container.css( 'opacity', 1 );
						},
						beforeSlideChangeStart: function () {
							var $total = container.find('.durotan-slider-item:not(.swiper-slide-duplicate)').length;

							$('.durotan-slide__fraction-total').text($total);
						},
						slideChange: function () {
							var $total = container.find('.durotan-slider-item:not(.swiper-slide-duplicate)').length,
								$current = this.realIndex + 1;

							$('.durotan-slide__fraction-current').text($current);
							$('.durotan-slide__fraction-total').text($total);
						},
					},
					
				};

		new Swiper( container, options );

	};

	durotan.popupVideo = function () {
		if ( ! $.fn.magnificPopup ){
			return;
		}

        $('.durotan-slide__play-video-button').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 300,
            preloader: false,

            fixedContentPos: false
        });
	};
	
	durotan.sliderTabs = function () {
		if ( typeof Swiper === 'undefined' ) {
			return;
		}

		$( document.body ).on( 'click', '.durotan-slide__image-swatches-link', function( e ) {
			e.preventDefault();

			var $tab = $( this ),
				$panels = $tab.parent().siblings( '.durotan-slide-price-wrapper' ),
				$panelsBg = $tab.closest('.durotan-slider-item').find( '.durotan-sliders-bg-wrapper' );

			if ( $tab.hasClass( 'active' ) ) {
				return;
			}

			$tab.addClass( 'active' ).siblings().removeClass( 'active' );
			$panels.children().eq( $tab.index() ).addClass( 'active' ).siblings().removeClass( 'active' );
			$panelsBg.children().eq( $tab.index() ).addClass( 'active' ).siblings().removeClass( 'active' );
		} );
	};

    /**
	 *    Fire when document ready
	 */
	$( function() {
		durotan.init();
	} );
})( jQuery );