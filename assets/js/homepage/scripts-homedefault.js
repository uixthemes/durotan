var durotan = durotan || {};

(function( $ ) {
    'use strict';
	/**
	 * The main function to init the theme
	 */
	durotan.init = function() {
		this.ProductHomeDefault();
		this.ProductHomeDefault_Image();

		this.tabsHomepage();
		this.colorProduct();
		this.sizeProduct();
		this.scrollTabs();
	};

	durotan.ProductHomeDefault = function() {
		if ( typeof Swiper === 'undefined' ) {
			return;
		}

		var container = $('.durotan-product__slider .durotan-product__slider__container'),
				options = {
					slidesPerView: 4,
					spaceBetween: 31,
					loop            : false,
					autoplay        : false,
					delay           : 800,
					speed           : 1000,
					watchOverflow   : true,
					lazy            : false,
					fadeEffect: {
						crossFade: true
					},
					hashNavigation: {
						watchState: true,
					},
					navigation: {
						nextEl: '.durotan-product__slider .durotan-product-button-next',
						prevEl: '.durotan-product__slider .durotan-product-button-prev',
					},
					pagination: {
						el: '.durotan-product__slider .durotan-product-pagination',
						clickable: true,
					},
					breakpoints: {
						0: {
							slidesPerView: 1,
						},
						480: {
							slidesPerView: 1,
						},
						768:{
							slidesPerView: 2,
						},
						992: {
							slidesPerView: 3,
						}
					}
				};
		new Swiper( container, options );
	};

	durotan.ProductHomeDefault_Image = function() {
		if ( typeof Swiper === 'undefined' ) {
			return;
		}

		$('.durotan-product__slider .durotan-product_slider__item').each(function(index){
			$(this).addClass('durotan-product_slider__item__'+index);
			var container = $('.durotan-product_slider__item__'+index+' .durotan-product__images-slider'),
					options = {
						slidesPerView: 1,
						spaceBetween: 50,
						loop            : false,
						autoplay        : false,
						delay           : 800,
						speed           : 1000,
						watchOverflow   : true,
						lazy            : false,
						fadeEffect: {
							crossFade: true
						},
						on              : {
							init: function() {
								container.css( 'opacity', 1 );
							}
						},
						navigation: {
							nextEl: '.durotan-product_slider__item__'+index+' .durotan-product-button-next__image',
							prevEl: '.durotan-product_slider__item__'+index+' .durotan-product-button-prev__image',
						},
					};
			new Swiper( container, options );
		});
	};

	durotan.tabsHomepage = function() {
		$('.durotan-product__tabs span').on( 'click', function() {
			$('.durotan-product__tabs span').removeClass('active');
			$(this).addClass('active');
		});
	};

	durotan.colorProduct = function(){
		$('.durotan-product__color--button span').on( 'click', function() {
			$('.durotan-product__color--button span').removeClass('active');
			$(this).addClass('active');
		});
	};
	durotan.sizeProduct = function(){
		$('.durotan-product__size span').on( 'click', function() {
			$('.durotan-product__size span').removeClass('active');
			$(this).addClass('active');
		});
	};
	durotan.scrollTabs = function(){
		$(window).on( 'load', function() {
			var totalWidth = 0;
			$('.durotan-product__tabs span').each(function(index) {
				totalWidth += $(this).width()+80;
			});
			$('.durotan-product__tabs .scroll-tabs .scroll-tabs__container').css('width', totalWidth);

			var totalWidths = 0;
			$('.durotan-follow-social__details .durotan-follow-social__details--item').each(function(indexs) {
				totalWidths += $(this).width()+80;
			});
			$('.durotan-follow-social__details').css('width', totalWidths);
		});
	};
    /**
	 *    Fire when document ready
	 */
	$( function() {
		durotan.init();
	} );
})( jQuery );