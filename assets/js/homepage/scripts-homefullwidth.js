var durotan = durotan || {};

(function( $ ) {
    'use strict';
	/**
	 * The main function to init the theme
	 */
	durotan.init = function() {
		this.ProductHomeFullWidth();
		this.ProductHomeFullWidth_Image();

		this.tabsHomepage();
		this.colorProduct();
		this.sizeProduct();
		this.scrollTabs();
	};

	durotan.ProductHomeFullWidth = function() {
		if ( typeof Swiper === 'undefined' ) {
			return;
		}

		var container = $('.durotan-product__slider .durotan-product__slider__container'),
				options = {
					slidesPerView: 5,
					slidesPerColumn: 2,
					spaceBetween: 15,
					loop            : false,
					autoplay        : false,
					delay           : 800,
					speed           : 1000,
					watchOverflow   : true,
					lazy            : false,
					fadeEffect: {
						crossFade: true
					},
					pagination: {
						el: '.durotan-product__slider .durotan-product-pagination',
						clickable: true,
					},
					breakpoints: {
						0: {
							slidesPerView: 1,
							slidesPerColumn: 1,
							pagination: {
								type: 'progressbar',
							}
						},
						480: {
							slidesPerView: 1,
							slidesPerColumn: 1,
							pagination: {
								type: 'progressbar',
							}
						},
						768:{
							slidesPerView: 2,
							slidesPerColumn: 2,
							pagination: {
								type: 'progressbar',
							}
						},
						1200: {
							slidesPerView: 4,
							slidesPerColumn: 2,
						}
					}
				};
		new Swiper( container, options );
	};

	durotan.ProductHomeFullWidth_Image = function() {
		if ( typeof Swiper === 'undefined' ) {
			return;
		}

		$('.durotan-product__slider .durotan-product_slider__item').each(function(index){
			$(this).addClass('durotan-product_slider__item__'+index);
			var container = $('.durotan-product_slider__item__'+index+' .durotan-product__images-slider'),
					options = {
						slidesPerView: 1,
						spaceBetween: 50,
						loop            : false,
						autoplay        : false,
						delay           : 800,
						speed           : 1000,
						watchOverflow   : true,
						lazy            : false,
						fadeEffect: {
							crossFade: true
						},
						on              : {
							init: function() {
								container.css( 'opacity', 1 );
							}
						},
						navigation: {
							nextEl: '.durotan-product_slider__item__'+index+' .durotan-product-button-next__image',
							prevEl: '.durotan-product_slider__item__'+index+' .durotan-product-button-prev__image',
						},
					};
			new Swiper( container, options );
		});
	};

	durotan.tabsHomepage = function() {
		$('.durotan-product__tabs span').on( 'click', function() {
			$('.durotan-product__tabs span').removeClass('active');
			$(this).addClass('active');
		});
	};

	durotan.colorProduct = function(){
		$('.durotan-product__color--button span').on( 'click', function() {
			$('.durotan-product__color--button span').removeClass('active');
			$(this).addClass('active');
		});
	};
	durotan.sizeProduct = function(){
		$('.durotan-product__size span').on( 'click', function() {
			$('.durotan-product__size span').removeClass('active');
			$(this).addClass('active');
		});
	};
	durotan.scrollTabs = function(){
		$(window).on( 'load', function() {
			var totalWidth = 0;
			$('.durotan-product__tabs span').each(function(index) {
				totalWidth += $(this).width()+80;
			});
			$('.durotan-product__tabs .scroll-tabs .scroll-tabs__container').css('width', totalWidth);
		});
	};
    /**
	 *    Fire when document ready
	 */
	$( function() {
		durotan.init();
	} );
})( jQuery );