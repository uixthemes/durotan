(function( $ ) {
    'use strict';
	/**
	 * The main function to init the theme
	 */
	durotan.init = function() {
		this.durotanCommit();
		this.ProductHomeModern();
		if( $(window).width() < 768 )
		{
			this.ScrollTabsProduct();
		}

		this.tabsHomepage();
		this.colorProduct();
		this.sizeProduct();
	};

	durotan.durotanCommit = function() {
		if ( typeof Swiper === 'undefined' ) {
			return;
		}

		var container = $('.durotan-commit-slider'),
				options = {
					slidesPerView: 'auto',
					spaceBetween: 15,
					breakpoints: {
						0:{
							slidesPerView: 'auto',
							spaceBetween: 0,
							pagination: {
								el: '.durotan-commit__pagination',
								clickable: true,
								type: 'progressbar',
							},
						},
						480:{
							slidesPerView: 'auto',
							spaceBetween: 0,
							pagination: {
								el: '.durotan-commit__pagination',
								clickable: true,
								type: 'progressbar',
							},
						},
						768:{
							slidesPerView: 2,
							pagination: {
								el: '.durotan-commit__pagination',
								clickable: true,
								type: 'progressbar',
							},
						},
					}
				};
		new Swiper( container, options );
	};
	durotan.ProductHomeModern = function() {
		if ( typeof Swiper === 'undefined' ) {
			return;
		}

		var container = $('.durotan-product__slider .durotan-product__slider__container'),
				options = {
					slidesPerView: 4,
					spaceBetween: 30,
					loop            : false,
					autoplay        : false,
					delay           : 800,
					speed           : 1000,
					watchOverflow   : true,
					lazy            : false,
					fadeEffect: {
						crossFade: true
					},
					hashNavigation: {
						watchState: true,
					},
					navigation: {
						nextEl: '.durotan-product__slider .durotan-product-button-next',
						prevEl: '.durotan-product__slider .durotan-product-button-prev',
					},
					pagination: {
						el: '.durotan-product__slider .durotan-product-pagination',
						clickable: true,
					},
					breakpoints: {
						0: {
							slidesPerView: 1,
							pagination: {
								type: 'progressbar',
							}
						},
						480: {
							slidesPerView: 'auto',
							spaceBetween: 20,
							pagination: {
								type: 'progressbar',
							}
						},
						768: {
							slidesPerView: 2,
							pagination: {
								type: 'progressbar',
							}
						},
						1024:{
							slidesPerView: 3,
							pagination: {
								type: 'progressbar',
							}
						},
					}
				};
		new Swiper( container, options );
	};
	durotan.ScrollTabsProduct = function() {
		if ( typeof Swiper === 'undefined' ) {
			return;
		}

		var container = $('.durotan-product-scroll-tabs'),
				options = {
					slidesPerView: 'auto',
					navigation: {
						nextEl: '.durotan-product__tabs--homemodern .scroll-next-mobile',
						prevEl: '.durotan-product__tabs--homemodern .scroll-prev-mobile',
					},
				};
		new Swiper( container, options );
	};
	durotan.tabsHomepage = function() {
		$('.durotan-product__tabs span').on( 'click', function() {
			$('.durotan-product__tabs span').removeClass('active');
			$(this).addClass('active');
		});
	};

	durotan.colorProduct = function(){
		$('.durotan-product__color--image img').on( 'click', function() {
			$('.durotan-product__color--image img').removeClass('active');
			$(this).addClass('active');
		});
	};
	durotan.sizeProduct = function(){
		$('.durotan-product__size span').on( 'click', function() {
			$('.durotan-product__size span').removeClass('active');
			$(this).addClass('active');
		});
	};
    /**
	 *    Fire when document ready
	 */
	$( function() {
		durotan.init();
	} );
})( jQuery );