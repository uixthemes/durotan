var durotan = durotan || {};

(function( $ ) {
    'use strict';
	/**
	 * The main function to init the theme
	 */
	durotan.init = function() {
		this.ProductHomeModern();

		this.tabsHomepage();
		this.colorProduct();
		this.sizeProduct();
	};

	durotan.ProductHomeModern = function() {
		if ( typeof Swiper === 'undefined' ) {
			return;
		}

		var container = $('.durotan-product__slider .durotan-product__slider__container'),
				options = {
					slidesPerView: 4,
					slidesPerColumn: 1,
					spaceBetween: 30,
					loop            : false,
					autoplay        : false,
					delay           : 800,
					speed           : 1000,
					watchOverflow   : true,
					lazy            : false,
					fadeEffect: {
						crossFade: true
					},
					navigation: {
						nextEl: '.durotan-product__slider .durotan-product-button-next',
						prevEl: '.durotan-product__slider .durotan-product-button-prev',
					},
					breakpoints: {
						0: {
							slidesPerView: 1,
						},
						480: {
							slidesPerView: 1,
						},
						768:{
							slidesPerView: 2,
						},
						1024:{
							slidesPerView: 3,
						},
					}
				};
		new Swiper( container, options );
	};

	durotan.tabsHomepage = function() {
		$('.durotan-product__tabs span').on( 'click', function() {
			$('.durotan-product__tabs span').removeClass('active');
			$(this).addClass('active');
		});
	};

	durotan.colorProduct = function(){
		$('.durotan-product__color--image img').on( 'click', function() {
			$('.durotan-product__color--image img').removeClass('active');
			$(this).addClass('active');
		});
	};
	durotan.sizeProduct = function(){
		$('.durotan-product__size span').on( 'click', function() {
			$('.durotan-product__size span').removeClass('active');
			$(this).addClass('active');
		});
	};
    /**
	 *    Fire when document ready
	 */
	$( function() {
		durotan.init();
	} );
})( jQuery );